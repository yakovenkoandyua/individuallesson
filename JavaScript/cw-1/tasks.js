/* ЗАДАНИЕ - 1
 * Пользователь вводит в модальное окно любое число. Нужно вывести на экран "Четное" или "Не четное" в зависимости от того четное число или нет.
 */


/* ЗАДАНИЕ - 2
 * Спросить пользователя на каком языке он хочет увидеть список дней недели. Пользователь может ввести  - ru, en, ukr
 * Вывести на экран список дней недели на выбранном языке.
 * */


/* ЗАДАНИЕ - 3
 * Пользователь вводит 3 числа. Нужно вывести на экран собщение с максимальным числом из введенных
 * */

/* ЗАДАНИЕ - 4
 * Имитируем список персонала, где у каждого человека есть своя роль. Пользователь вводит имя, после чего нужно вывести сообщение с ролью пользователя.
 * Список следующий:
 * Boss - главный Boss
 * Boss Juniior - правая рука Boss'a
 * Gogol Mogol - далает Гоголь-Моголь всем сотрудникам
 * John - уборщик
 * */



/* ЗАДАНИЕ - 5
 * Напишите программу кофейная машина.
 * Программа принимает монеты и готовит напиток (Кофе 25грн, капучино 50грн, чай 10грн).
 * Т.е. пользователь вводит в модальное окно сумму денег и название напитка.
 * В зависимости от того какое название было введено - нужно вычислить сдачу и вывести сообщение: "Ваш напиток *НАЗВАНИЕ НАПИТКА* и сдача *СУММА СДАЧИ*"
 * В случае если пользователь ввел сумму без сдачи - выводить сообщение: "Ваш напиток *НАЗВАНИЕ НАПИТКА*. Спасибо за точную сумму!))"
 * */

