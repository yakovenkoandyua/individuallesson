const name = 'ivan';
const lastName = 'ivanov';
const age = 25;

const userArray = ['ivanov', 'ivan', 25];

const userObject = {
	name: 'ivan',
	lastName: 'ivanov',
	age: 25,
	job: 'Dan-IT',
	groups: ['pe37', 'fs3', 'fem5'],
	awardSalary: false,
	courses: {
		frontEnd: 'html/css, js',
		backEnd: 'nodeJs, php',
	},
	setAge: (firstNumber, secondNumber) => {
		return firstNumber + secondNumber;
	},
};

// console.log(userObject.setAge(2, 3));
// console.log(userObject['name']);

// const createUser = (name, age, lastName) => {
// 	return {
// 		name,
// 		age,
// 		lastName,
// 	};
// };

// console.log(createUser(12, 'vanya', 'иванов'));


